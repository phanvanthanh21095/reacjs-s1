import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import style from './ListCourses.module.scss';
import config from '../../config';
import createInternalUrl from '../../unti/createInternalUrl';

class ListCourses extends Component {
  static propTypes = {
    listCourses: PropTypes.array.isRequired,
  }

  static defaultProps = {
    listCourses: [],
  }

  renderlistCourses = () => {
    return this.props.listCourses.map(course => {

      const url = createInternalUrl(config.router.coursesDetails, {
        slug: course.slug
      })
      return (
        <li key={course.id} className={style.box_content}>
          <h3>{course.name}</h3>
          <p>{course.description}</p>
          <p>{course.coin}</p>
          <Link to={url} className={style.btn_create}>Hoc ngay</Link>
        </li>
      )
    })
  }
  render() {
    return (
      <ul className={style.box_content_courses}>
        {this.renderlistCourses()}
      </ul>
    );
  }
}

export default ListCourses;
