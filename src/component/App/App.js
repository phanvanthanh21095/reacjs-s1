import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import HomeContainer from "../../container/Home";
import ListCoursesContainer from "../../container/ListCourses";
import CoursesDetailContainer from "../../container/CoursesDetail";
import NavBarComponent from "../../component/NavBar";
import config from '../../config';

function App() {
  return (
    <Router>
      <Route>
        <NavBarComponent />
        <Switch>
          <Route path={config.router.home} exact component={HomeContainer} />
          <Route path={config.router.listCourses} component={ListCoursesContainer} />
          <Route path={config.router.coursesDetails} component={CoursesDetailContainer} />
        </Switch>
      </Route>
    </Router>
  );
}

export default App;
