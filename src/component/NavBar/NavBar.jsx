import React, { Component } from "react";
import { NavLink } from 'react-router-dom';
import config from '../../config';
import style from './NavBar.module.scss';

class NavBar extends Component {
    render() {
        return (
            <ul className={style.navBar}>
                <li className={style.navBar_item}>
                    <NavLink exact activeClassName={style.active} to={config.router.home}>Home</NavLink>
                </li>
                <li className={style.navBar_item}>
                    <NavLink activeClassName={style.active} to={config.router.listCourses}>ListCoures</NavLink>
                </li>
            </ul>
        );
    }
}

export default NavBar;
