import pathToRegexp from 'path-to-regexp';

export default function createInternalUrl(path, params) {
    const toPath = pathToRegexp.compile(path);
    return toPath(params);
}