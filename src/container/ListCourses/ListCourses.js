import React, { Component } from "react";
import ListCoursesComponent from '../../component/ListCourses';
import Api from '@mycv/mycv-api';
import config from '../../config';

const client = new Api(config.api);

class List extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listCourses: []
        }
    }

    componentDidMount() {
        this.getListCourses();
    }

    getListCourses = async () => {
        try {
            const response = await client.send(config.rest.listCourses());
            console.log('response', response);
            this.setState({
                listCourses: response.data
            })
        } catch (error) {
            console.log('error', error);
        }
    }

    render() {
        return <ListCoursesComponent listCourses={this.state.listCourses} />;
    }
}

export default List;
