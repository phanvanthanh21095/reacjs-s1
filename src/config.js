const config = {
    router: {
        home: '/',
        listCourses: '/courses',
        coursesDetails: '/courses/:slug'
    },

    api: {
        basePath: "https://gateway.fullstack.edu.vn",
        defaultRequestOptions: {
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }
    },

    rest: {
        listCourses: () => `/api/v1/courses`,
        CoursesDetail: slug => `/api/v1/courses/${slug}/public`
    }
}

export default config;